package tahlilsnaptaxi;

public class Driver extends Person{
    private int driverid;
    private int carid;
    private long accountcredit;
    
    public Driver(String name,String username,String password,String phonenumber,String location,int Driveid,int carid,int accountcredit){
        super(name,username,password,phonenumber,location);
        this.driverid=Driveid;
        this.carid=carid;
        this.accountcredit=accountcredit;
    }
    
    
    public void addcredit(long i){
        accountcredit+=i;
    }
    
    public String getusername(){
        return super.getusername();
    }
    
    public String getupassword(){
        return super.getpassword();
    }
    
    public String getphnumber(){
        return super.getphnumber();
    }
    
    public String toString(){
        return super.toString()+"\nDriver ID:"+driverid+"\nCar ID:"+carid+"\nAccount credit:"+accountcredit;
    }
    
}
