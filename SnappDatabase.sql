BEGIN;

--     SNAPP TAXI    --


--User table

CREATE TABLE "user" (
    "id" integer NOT NULL PRIMARY KEY , 
    "userName" varchar(200) NOT NULL, 
    "lastName" varchar(300) NOT NULL,
    "fathersname"varchar(50) NOT NULL,
    "phoneNumber" int NOT NULL, 
    "email" varchar(200) NOT NULL, 
    "sex" varchar(20) NOT NULL, 
    "birthday" datetime NOT NULL, 
    "location" varchar(200) NOT NULL, 
    "age" integer NOT NULL
);


-- driver table 

CREATE TABLE "driver" (
    "id" integer NOT NULL PRIMARY KEY , 
    "firstName" varchar(200) NOT NULL, 
    "lastName" varchar(200) NOT NULL,
    "fathersname"varchar(50) NOT NULL,
    "email" varchar(200) NOT NULL,  
    "hiringDate" datetime NOT NULL, 
    "phoneNumber" integer NOT NULL,
    "birthday" datetime NOT NULL,
    "sex" varchar(20) NOT NULL,
    "lastJob" varchar(200) NOT NULL,
     "drivinglicenceCode" int not null, 
);

-- drive table 

CREATE TABLE "drive" (
    "id" integer NOT NULL PRIMARY KEY,
    "travelStartedTime" TIME not null,
    "travelFinishedTime" TIME not null,
    "travelOriginLocation" varchar(200) not null,
    "travelDsetinationLocation" varchar(200) not null,
    "driveCost" int not null,

);

-- drivers box table 

CREATE TABLE "driverBox" (
    "id" integer NOT NULL PRIMARY KEY,
    "driverScore" int not null,
    "ComplainAgainstDriver" text not null,
    "Criticisms" text not null,
    "goodPoints" text not null,
);

-- passenger box table 

CREATE TABLE "passengerBox" (
    "passengerScore" int not null,
    "ComplainAgainstpassenger" text not null,
    "Criticisms" text not null,
    "goodPoints" text not null,
);

-- payment table 

CREATE TABLE "payment" (
    "email" VARCHAR(200) not null,
    "number" int not null,
    "cardInfo" text not null,
    "Cost" int not null,
    "trackingCode"int not null,
    "paymentTime" time not null,
    "paymentDate" date not null,  
);
 
 -- profile table 

CREATE TABLE "profile" (
    "email" VARCHAR(200) not null,
    "number" int not null,
    "favoriteLocation" VARCHAR(350) not null,
    "allTheCost" int not null,
    "allThetrackingCode"int not null,
    "allDriveInfo" text(3000) not null,
    
);

-- driver's Car table 

CREATE TABLE "car" (
    "model" varchar(200) not null,
    "carID" int not null,
    "colour" varchar(10) not null,
    "carTag"  int not null,
    
);

-- contact Us table 

CREATE TABLE "contactUsBox" (
    "email" varchar(100) not null,
    "phoneNumberForCallFromOperator" int not null,
    "textBox" text not null,
    "reportBox" text not null,
);


COMMIT;
