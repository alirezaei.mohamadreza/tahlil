package tahlilsnaptaxi;

public class Costumer extends Person{
    private long accountcredit;
    public Costumer(String name,String username,String password,String phonenumber,String location,long credit){
        super(name,username,password,phonenumber,location);
        this.accountcredit=credit;
    }
    
    public void addcredit(long i){
        accountcredit+=i;
    }
    
    public String getusername(){
        return super.getusername();
    }
    
    public String getpassword(){
        return super.getpassword();
    }
    
    public String getuphnumber(){
        return super.getphnumber();
    }
    
    public String toString(){
        return super.toString()+"\nAccount credit:"+accountcredit;
    }
    
}
