package tahlilsnaptaxi;

public class Person {
    private String name;
    private String username;
    private String password;
    private String phonenumber;
    private String location;
    
    public Person(String name,String username,String password,String phonenumber,String location){
        this.name=name;
        this.username=username;
        this.phonenumber=phonenumber;
        this.password=password;
        this.location=location;
    }
    
    public void setlocation(String location){
        this.location=location;
    }
    
    public void editPerson(String name,String username,String password,String phonenumber,String location){
        this.name=name;
        this.username=username;
        this.phonenumber=phonenumber;
        this.password=password;
        this.location=location;
    }
    
    public String toString(){
        return "Name:"+name+"\nUsername:"+username+"\nPassword:"+password+"\nPhone number:"+phonenumber+
                "\n Address:"+location;
    }
    public String getusername(){
        return username;
    }
    
    public String getpassword(){
        return password;
    }
    
    public String getphnumber(){
        return phonenumber;
    }
    
}
